# Project Plan:

This workplan contains tasks in the following scope:

* Tools to produce a source distribution and multi-architecture Forgejo binaries

  The target architectures of Forgejo binaries are amd64, arm64, armv7 and armv6. Tools are developed to allow the creation of binaries for all architectures on a single amd64 machine. They include testing of the distribution using the native architecture to verify it actually works as intended. The toolchain that will be used is distributed as OCI images and Forgejo OCI images can also conveniently be created from them.

  The resulting artifacts (sources, binaries and OCI images) are used by the Free Software ecosystem to package Forgejo. Creating packages for distributions is not in the scope of the Forgejo project. The binaries distributed by Forgejo serve two purposes: they can be used standalone in production and in a test environment, from the most basic unit testing to high level end-to-end testing.

* An integrated release pipeline based on Forgejo

  A release pipeline based on continuous integration is developed to allow for building a Forgejo release for all targetted packages and architectures. The release manager pushes a tag to the continuous integration system hosted on a Forgejo instance to trigger the pipeline that:

  * Runs security checks
  * Runs unit and integration tests
  * Builds the OCI images
  * Builds the binaries
  * Uploads OCI images and binaries to a designated Forgejo instance

  the pipeline is also desginged to be able to copy releases from one Forgejo instance to another to allow for multi-stage releases for release candidates or experimental releases.

* A new continous integration agent

  A continous integration agent runs the workload for all continuous integration tasks, which includes the release pipeline itself. It is developed to be systemd capable, include nested hardware virtualization and nested system containers. The nested hardware virtualization allows integration test to be run even when they require KVM, for instance when testing the infrastructure as code to deploy Forgejo. The nested containers allows testing the continuous integration agent itself. The agent allows running workloads in LXC containers which are systemd capable to test the OCI images can effectively be used, for instance with database installed from packages native of a given distribution.

* Tooling and documentation to maintaing a soft fork

  As soft fork is a set of patches applied on top of an existing codebase. When the codebase changes, the same set of patches must be applied again on top of the new codebase. It may no longer work because the patch conflicts or because the features changed. As time passes and the number of patches grow, manually applying them again becomes increasingly difficult. Tools are developed to facilitate this work and best practices are documented to keep the maintenance burden to a minimum.

## Structure

The project plan is made of multiple tasks, which are divided in milestones. A task should have a explanatory text summarizing what will be done in words that are understandable to people outside your project, and any conditions or external interactions. Each milestone should be a bullet point, indicating a concreate outcome ("deliverable"). Amount can be specified at the milestone or task level.

Example:

```
## Title of some task (€ 4000)

One or two lines of what the task is about (including any subtasks) will do to help everyone understand what the project aims to achieve in this chunk of work.

- Create requirements document + threat modelling (€ 500)
- Implement technical design (€ 3000)
- First alpha release (€ 500)

## Descriptive title of another task (€ 1500)

Rinse and repeat. For the remaining tasks as well one can surely produce a few sequences of words to add that wonderful feeling of structure and oversight. This tasks will achieve features X and Y, allowing users to do Z.

- Certified WCAG accessible
- New beta release with feature X and Y
```

Regarding the amounts, the total request amount in 50,000 € (with an hourly rate of 60 €).

# Tasks

## Scope: Tools to produce a multi-architecture Forgejo distribution

### Tools to produce the binary Forgejo distribution

The Forgejo binary is built using a specific set of flags for the Go compiler. The resulting binary is tested to work using qemu architecture virtualization and binfmt.

Deliverable:
* URL to merged pull requests in https://codeberg.org/forgejo/forgejo

Milestone(s)
* Script to build binaries for amd64, arm64, armv6
* Support for GPG signing the binaries
* Tests for the script running the build
* CI configuration including the script
* Tests to verify the binary works on the target architecture

Amount:
    10%

### Tools to produce the OCI container Forgejo images

The Forgejo container image is built using buildx to support multiple platforms. The resulting image is tested to contain a usable binary image using the qemu architecture virtualization leveraged by docker and binfmt.

Deliverable:
* URL to merged pull requests in https://codeberg.org/forgejo/forgejo

Milestone(s)
* Script to build OCI images for amd64 and arm64
* Tests for the script running the build
* CI configuration including the script
* Tests to verify the image can run Forgejo on the target architecture

Amount
    10%

## Scope: An integrated release pipeline based on Forgejo

### Tag based release configuration

The release manager pushes a tag to the continuous integration system hosted on a Forgejo instance to trigger the pipeline.

Deliverable:

  * URL to merged pull requests in https://codeberg.org/forgejo/forgejo

Milestone(s):
  * Runs security checks
  * Runs unit and integration tests
  * Builds the OCI images
  * Builds the binaries
  * Uploads OCI images and binaries to a designated Forgejo instance

Amount
    5%

### DNS-Update-Checker RFC

___Note: this task was subsequently replaced. See below.___

Aprivacy-friendly and scalable system to check if an update is available (based on DNS)

Problems:
- the original (gitea) update-checker system leaked the IP-Address of the checking server
- the original update-checker relies on one https server (no trivial to scale)
- the Forgejo update-checker (based on DNS) started as an experiment, but could be standardized to benefit other projects

Deliverables:
- a RFC defining the goal and protocol details
- a client side implementation in Go, for integration in Forgejo

### Modular webhook logic

___Note: This task replaced the update-checker task above by agreement with NLNet on 2024-07-04.___

The webhook logic is currently intricate and there is no clear separation between the various webhook types.

Problems:
- special cases for specific webhooks are mixed in the general webhook code
- adding a new webhook type requires changes in the general webhook code
- lot of code duplication for every supported webhook type

Milestones:
- Refactor webhooks under a common interface
- Use the common interface for managing the webhook settings endpoints
- Generalize the webhook templates to use the common interface

Amount:
   10%

### Cleaner Webhook system

Webhooks are the main way to communicate with other systems. Having a clean system will allow for other webhooks to be developed and Forgejo to better integrate in the wider forge ecosystem.

Problems:
currently the webhook is a two-step process: a part of the payload is generated just after the webhook-event happened; the rest of the payload is defined just before send the actual request.
- this two-step process makes the current code very intricate and difficult to maintain
- it will help move forward the [Custom webhook pull request](https://github.com/go-gitea/gitea/pull/19307), since it will reduce its scope.

Deliverable:
- a pull request implementing the webhook system in one step (after the webhook-event)
- a pull request implementing a webook to https://builds.sr.ht/

Amount:
   10%

## Scope: A new continous integration agent

### LXC helpers

LXC provides low level system containers which are used by Forgejo in
various contexts, from the CI to end-to-end testing of a release
before it is published. The patterns common to all are grouped in
helpers that are released and tested independently.

Deliverable:
   * URL to merged pull requests in https://code.forgejo.org/forgejo/lxc-helpers

Milestone(s):
   * Helper to create & destroy a container, with a dedicated user and support for nesting them
   * Integration tests to verify each helper
   * Performance improvements to reduce the creation time of a container
   * Tagged release of the helpers

Amount:
    5%

### Forgejo runner

The Forgejo runner is at the heart of Forgejo and its release pipeline. The nested containers allows testing the continuous integration agent itself. The agent allows running workloads in LXC containers which are systemd capable to test the OCI images can effectively be used, for instance with database installed from packages native of a given distribution.

Deliverable:
  * URL to merged pull requests in https://code.forgejo.org/forgejo/runner
  * URL to merged pull requests in https://code.forgejo.org/actions/setup-forgejo

Millestone(s):
   * A binary that can be used as an agent for the Forgejo Actions CI
   * An Action that deploys Forgejo and the runner
   * Tests that verify the runner works in a nested system environment

Amount:
    20%

### LXC backend

The continuous integration agent is based on ACT which allows running workloads on a local machine. It is based on Docker and does not support nesting or systemd. An LXC backend is added to support both.

Deliverable:
   * URL to merged pull requests in https://code.forgejo.org/forgejo/act

Milestone(s):
   * Release that includes the LXC backend
   * Support for nested hardware virtualization (KVM)
   * Support for nested system containers

Amount:
    10%

## Scope: UI and accessibility improvements

The Forgejo UI has developed over time (first as Gogs and then Gitea) in an unstructured, ad-hoc way, lacking semantic structure. This makes UI consistency and good accessibility difficult to achieve and maintain. In order to improve accessibility, it will be necessary to improve the semantic structure of the UI.

### Improve modularity of the UI

Forgejo currently has some reusable UI components, but most of the UI is not modular. This significantly increases the maintainability burden and makes accessibility much harder.

Deliverables:
  * Create reusable components for common elements that can be used throughout the UI.
  * Ensure that each component is accessible and based on semantic markup.

Amount: 5%

### Semantic theming support

The current theming support in Forgejo has many hooks (variables and class names) which are based on appearance instead of semantics, or which are abused for multiple different purposes, making accessible theming difficult. In order to ensure that themes can be made properly accessible, theming support must be reworked in a more semantic way, allowing targeting of elements based on role and UI structure instead of simple colour replacements.

Deliverables:
  * Restructure the most frequently used parts of the UI with semantic use of class names and CSS variables.
  * Rewrite themes to improve accessibility by using new semantic hooks.

Amount: 5%

### Ensure accessibility without JavaScript

The legacy Forgejo UI leans heavily on Fomantic-UI, a jQuery-based framework with little in the way of accessibility support. Whilst some workarounds are currently in place, a better long-term solution would be to migrate away from Fomantic-UI and jQuery.
Where appropriate, parts of the UI which use Fomantic could be migrated to Vue, which is used elsewhere in the UI, or to other frameworks or vanilla JavaScript. Progressive enhancement should be used to ensure all parts of the UI work with JavaScript disabled.

Deliverables:
  * Reduce the use of Fomantic-UI and jQuery by replacing with other solutions on the most frequently used pages.
  * Use progressive enhancement to ensure the most frequently used parts of the UI work without JavaScript.
  * Replace the issue/comment editor with a more accessible solution which also works on mobile browsers.

Amount: 7.5%

### Fix accessibility issues identified by an audit

After fixing the accessibility issues discussed above, request an accessibility audit via NGI Zero Review.

Deliverables:
  * Ensure WCAG level AA compliance of the most important parts of the UI when using the default light and dark themes.
  * Stretch goal: attempt to ensure WCAG level AAA compliance where possible.

Amount: 2.5%
