# sovereign_tech

# Start here

## Application name

Forgejo

# Project Description

## Project title: 

Forgejo

## Describe your project in a sentence. (100)

Forgejo is a community-driven free software forge that builds on previous experiences with similar projects, aiming to provide a sustainable and transparent environment for developers by enhancing governance structures and ensuring community ownership and collaboration.

## Describe your project more in-depth. Why is it critical? (300)

Forgejo originated in October 2022 in response to the for-profit takeover of former community project Gitea, and was initiated publicly by previous Gitea contributors. In early 2024, a community agreement was reached to develop Forgejo into a [hard fork of Gitea](https://en.wikipedia.org/wiki/Gitea#Forgejo_fork). 

In 2024 Forgejo is used as a collaborative development platform by over 50,000 free and open source software projects. There are over 100,000 registered Forgejo users and thousands of self-hosted instances. Forgejo supports many kind of source code, for instance to support computer applications, websites, publication papers, artistic work, ACE (Architecture, Engineering, Construction). It is easy to deploy and lightweight, has an integrated CI (Continuous-Integration) and can be self-hosted on inexpensive hardware.

In the past decades, and despite numerous attempts, software forges developed in the interest of the general public failed to become sustainable (phabricator, Allura, ...). In order to avoid that fate, and instead to become a self-sustaining and durable project providing software developers with lasting autonomy over their own tools, Forgejo builds on its contributors' valuable experience acquired in comparable projects. This includes developing the project in total transparency, a strong focus on community ownership, keeping balance between paid and unpaid contributors, and concerted efforts to expand the community of contributors. Furthermore, Forgejo is the only software community project working towards forge federation, a mechanism that allows parallel development of the source code in different forges. 

All these aspects are critical to further consolidate genuine community-led efforts to maintain control over developers' tools, thus significantly reducing the vulnerability to for-profit takeovers, and providing an actual long-term solution to the development of free and open source software in the public interest. Furthermore, timing of the activities proposed here is critical, as the project finds itself in a stage in which it requires an extra push to establish sustainable structures.

## Link to project repository

https://codeberg.org/forgejo

## Link to project website

https://forgejo.org/

## Please provide a brief overview over your project’s own, most important, dependencies. (300) This means code or software that your project uses.

All upstream dependencies are exclusively free and open source software and the development life-cycle does not depend on any third party proprietary services. Upstream dependencies are managed with dedicated tooling and Forgejo contributors care for them on a regular basis.

- Development - all tools and services used for Forgejo development are considered upstream dependencies of Forgejo because its development cannot move forward without them.
    - Forgejo is the collaborative platform used by Forgejo developers https://codeberg.org/forgejo/forgejo
    - Localization uses Weblate https://translate.codeberg.org/projects/forgejo/
    - Forgejo has its own Continuous Integration software called Forgejo Actions and an ecosystem of plugins similar to GitHub Actions, all of which are verified to be free and open source software
- Distribution - binaries, OCI images and other release artifacts maintained by Forgejo are distributed on a Forgejo instance instead of a third party service such as the Docker Hub
    - OCI images downloads https://codeberg.org/forgejo/-/packages/container/forgejo/versions
    - Binary downloads https://codeberg.org/forgejo/forgejo/releases
- Runtime - Forgejo is a standalone binary that includes all its Go and JavaScript dependencies
    - Go https://codeberg.org/forgejo/forgejo/src/branch/forgejo/go.mod
        - infrastructure (gitea, chi, cors, swagger, gocron, ...)
        - communications (ssh, grpc, protobuf, activitypub, ldap, httpsig, otp, imap, RSS ...)
        - authentication (webauthn, goth, go-crypto, pam, OpenID ...)
        - database (MySQL, PostgreSQL, SQLite, ...)
        - search (meilisearch, bleve, elasticsearch)
        - VCS (git)
        - Object storage (minio, garage)
        - UI components (go-enry, editorconfig, goldmark, ...)
    - JavaScript https://codeberg.org/forgejo/forgejo/src/branch/forgejo/package.json
        - frameworks (vue, tailwind, ...)
        - modules (cssloader, postcss, ...)
        - UI components (asciinema-player, mermaid, pdfobject, ...)

## Please provide a brief overview of projects that depend on your technology. (300)

- Packages are maintained for various distributions channels https://codeberg.org/forgejo-contrib/delightful-forgejo#packaging (YunoHost, Alpine, Arch, Debian GNU/Linux, NixOS, Snapcraft, ...)
- Over 50,000 free and open source software projects use Forgejo in their development life cycle, for collaboration and distribution. Forgejo can be considered an integral part of these software rather than a third party service.
    - On self-hosted instances
    - On Codeberg https://codeberg.org/explore/repos?sort=moststars
    - On other instances with public registration https://codeberg.org/forgejo-contrib/delightful-forgejo#public-instances

## Which target groups does your project address (who are its users?) and how would they benefit from activities proposed for funding (directly and indirectly)? (300)

Immediate beneficiaries include

* Software developers assisting Human Rights Defenders (HRDs):
    * They can work together using cheap hardware and Forgejo as a lightweight collaborative platform in a secure network.
    * It is the workbench where they create or modify software to evade surveillance in oppressive regimes and combat censorship using techniques specific to their regional context.
    * It allows them to run tests, create and distribute quality releases to reduce the risk of security vulnerabilities when they are used in the field.
    * Its ease of deployment allows for creative ways to prevent surveillance, for instance a local deployment not connected to the Internet, tor hidden service or in a country where laws are less of a threat to privacy.
* Software developers - work together on a software writing code, testing, packaging and deploying.
* System administrators - install, upgrade and address security issues for the benefit of the users.
* Software users - download packaged software, report bugs and discuss new features.
* Service providers - bridge the gap between software users with no technical expertise and the software developers.

Indirect beneficiaries include

* human rights defenders depending on software developed, distributed and maintained on Forgejo instances only accessed via tor, by way of making solid, modifiable, accessible tools more readily available and promoting technical independence.
* users of a software developed and maintained on Forgejo.
* the general public, by promoting independent community exchange, volunteering approaches and the ethics and approaches of the free and open source software community.

Demographics

* Most users are drawn to Forgejo for ethical reasons.
* World-wide with a majority based in Europe.
* Most developers who participate maintain or contribute to a free and open source software code base.
* Most system administrators are volunteers.
* Service providers are small companies advertising free and open source software expertise.

See also the [User Research repository](https://codeberg.org/forgejo/user-research).

## Please describe a specific scenario for the use of your technology and how this meets the needs of your target groups. (300) 

The described scenario refers to the target group of free and open source software developers. 

[Gadgetbridge](https://gadgetbridge.org/) is a free and open source Android application that allows users to pair and manage various gadgets such as smart watches, bands, headphones, and more without the need for the vendor application. As a result, Gadgetbridge can be used as an open source alternative to a gadget's own proprietary app.

It is [developed and distributed using Forgejo](https://codeberg.org/Freeyourgadget/Gadgetbridge). Forgejo is used by Gadgetbridge to:

* Submit and discuss feature requests
* Improve the software in collaboration with a distributed community
* Provide ready to use packages
* Project planning
* Group related projects together
* Organize teams and delegate permissions

## How was the work on the project made possible so far (structurally, financially, including volunteer work)? If applicable, list others sources of funding that you applied for and/or received. (300)

Forgejo is driven by a variety of independent actors:

* Codeberg, custodian of the domain - dedicated to operating a single instance hosting over 100,000 projects and developers
* Volunteers
* Employees delegations
* Independent contractors paid by their clients
* Individuals funded by grants

Currently, the bulk of the work on the project is made possible by volunteer contributors: They achieve an estimated 75% of the workload, while the remaining 25% are done by individuals compensated for their contributions as independent contractors or by their employers. In the interest of transparency, individuals contributing as independent contractors usually keep worklogs accessible to the community (see for example: https://codeberg.org/algernon/codeberg-worklog).

There is no notion of a core team in Forgejo. Instead there are a number of [teams](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md) composed of people interested in contributing actively to a given topic. The most active members in each team are what could be considered core team members in other projects. Only they do not have special privileges, they are bound to the same [decision making process](https://codeberg.org/forgejo/governance/src/branch/main/DECISION-MAKING.md) as all other contributors.

* https://codeberg.org/fnetX 
* https://codeberg.org/earl-warren 
* https://codeberg.org/0ko 
* https://codeberg.org/algernon 
* https://codeberg.org/viceice - https://github.com/viceice 
* https://codeberg.org/caesar 
* https://codeberg.org/jerger 
* https://codeberg.org/patdyn 
* https://codeberg.org/Gusted 
* https://codeberg.org/thefox

Every month all contributors are listed in a report that tells the story of how they collectively made progress: https://forgejo.org/tag/report/. The participation grew significantly in 2024 with over 100 contributors monthly. Forgejo inherited its general workflow from GitHub. It uses forks and pull requests for the purpose of collaboration. 

In 2022, the project received a grant over 50,000€ from NLnet Foundation to further forge federation. In 2023, NLnet Foundation provided 50,000€ for work on the release process and the code base.

An application submitted to Open Technology Fund’s Free and Open Source Software Sustainability Fund in May 2024 was dismissed. 

## What are the challenges you currently face in the maintenance of the technology? (300) Please interpret this question as broadly as you want. A non-conclusive lists of challenge areas we are interested in learning about are: managing contributions, addressing security vulnerabilites, project governance, licencing and other legal issues, dependency management, issue backlog, long-term planning/roadmapping.

Critical challenges 

* Roadmap - in its absence:
    * users, administrators don't know if Forgejo will match their use case.
    * contributors waste energy figuring out how to maximize impact.
* User Research - UI and UX are implemented ineffectively.
* Recruiting contributors - New users generate a growing workload.
* Security audit - The capacity of the security team doesn't accomodate the needs of HRDs.
* Test - Insufficient tooling & coverage.
    * The test suite is difficult to use.
    * Large parts of the code base still lack coverage.
* Funding - Forgejo is governed by a majority of volunteers who also accomplish an estimated 75% of the work alongside a paid staff of three people. There is not enough funding to employ additional paid staff to maintain that balance.
* Data portability - The code to migrate to/from other forge software only covers part of the data they contain.
* Documentation - Content and tooling are missing,
    * there is almost no tooling.
    * user and admin guides are only at 50% completion.
* Bugs backlog - There are currently over 300 reported bugs; the number keeps growing.
* Localization - As the Forgejo localization team created early 2024, the quality of over 5000 strings have not been verified.
* Scaling out - Federation is not implemented and requires Forgejo to scale up instead of scaling out. 

Non-critical challenges

* Technical debt - Fundamental parts of the code base need refactoring.
* Accessibility - WCAG audits must be conducted.
* Packaging and distribution - Some architectures, operating systems, packages and distributions channels are still missing support.
* Moderation and well-being - Disruptive behavior needs to be dealt with in a timely manner.
* Promotion, adoption & integration - Software developers and potential users are unaware of Forgejo's existence.

## What are possible alternatives to your project and how does your project compare to them? (optional) (300)

Forgejo builds on its contributors' previous experiences with similar open source community projects and software forges, such as Gitea and Phabricator, which have since become unsustainable or proprietary. Forgejo follows the same initial mission of providing developers of free and open source software with a workbench for their tools in a community-driven environment. However, it has established safeguards to avoid the fate of similar projects. To this end, Forgejo continually improves its governance structures to ensure unrestricted community ownership and contributor sovereignty, transparency, inclusive exchange and collaboration.

In its current setup, Forgejo promotes a unique approach. One of the main goals of the activities proposed here is to further consolidate this uniqueness by establishing an independent structure for fund distribution that is 100% controlled by the community.

# Scope of work

## What do you plan to implement with the support from STF? (900) Please describe your objectives and the corresponding activities. State clearly how they contribute to the improvement or maintenance of the technology, especially with regard to security and resilience.

Scope of work - overview 

* Maintenance 
    * Usability and accessibility improvements with User Research - This will contribute to expanding the user base, thus making Forgejo a more widely accepted tool.
    * Roadmap - Providing a strategic perspective for immediate and mid-term project development increases the project's resilience, including beyond achieving the set of activities outlined here.
    * Security audit - An external security review reveals possible weaknesses.
    * Data portability - Supports the establishment of federated forges, thus strengthening long term sustainability of the project's approach.
    * Bugs backlog - Clearing bugs improves overall usability of the software.
* Operations 
    * Testing - Test are necessary to review whether software is in fact working as designed.
    * Documentation - Current documentation has grown organically and is incomplete. Closing gaps and streamlining the content of documentation increases Forgejo's accessibility to new contributors.
    * Funding and long-term strategic planning - To make sure the project can last beyond the period of activities proposed here, and to maintain efforts in establishing forge federation.
* Community 
    * Recruiting Contributors - To ensure sustainability, the number of contributors to Forgejo should grow 10 times in the next five years. To achieve this, concerted efforts of outreach and promotion are necessary.
    * Localization - Facilitates adoption of a variety of users across the globe.

Objectives

* Keep organizational costs (administrative, accounting, etc.) under 10%.
* A non-profit organization governed by all current Forgejo stakeholders manages the grant and long-term funding. 
* User Research is conducted every year and made available with reports and raw data.
* Roadmaps are published
    * for the next 12 months and updated monthly. 
    * for long term objectives and updated yearly.
* A long term funding strategy is published and updated yearly.
* The long term funding strategy is implemented within a year. 
* The bug backlog is cleared within two years.
* Security audits are 
    * conducted every year.
    * the most pressing recommendations are acted upon within a year.
* The user and admin documentation are completed within a year and continuously updated.
* Within two years tests are 
    * refactored.
    * cover at least 75% of the code base.
* Supported translations 
    * are reviewed within two years. 
    * each supported language has a reviewer within a year. 

Activities

Forgejo progress toward long term sustainability is measured monthly with:
* A monthly report similar to https://forgejo.org/tag/report/ 
* Additional verifiable quantitative metrics 

The timeline for the first year is:
* Q1 
    * Establish a legal entity to sign the grant and receive funds
    * Recruit part time organizational staff
    * Recruit User Research paid staff
    * Conduct the first round of User Research focused on sustainability issues
    * Define a 12 months roadmap based on the User Research report

* Q2 
    * Recruit volunteers and paid staff to implement the roadmap (technical writer, fundraiser, developer, UX designer, etc.)
    * Implement the 12 months roadmap 
    * Conduct a security audit
    * Define a funding strategy

* Q3 & Q4 
    * Implement the 12 months roadmap 
    * Act on the recommendations of the security audit 
    * Define a long term roadmap 
    * Implement the funding strategy 
    * Define and document recurring activities addressing sustainability issues (funding, security audits, documentation, etc.) 
    * Review project progress and adapt the strategy accordingly 

The timeline for the second year follows the guide published to define activities related to sustainability.

Deliverables

* Pull requests 
    * clearing the bug backlog 
    * refactoring tests 
    * improving documentation tooling 
    * improving documentation 
    * updating the teams with new contributors 
    * increasing test coverage to 75% 
    * increasing data portability 
    * user research reports & data 
    * implementing federation 

* Monthly reports with quantitative and auditable measures related to: 
    * active contributors (paid staff & volunteers) 
    * localization 
    * number of bugs fixed 
    * test coverage 
    * funding and expenses 
    * new and updated documentation items 
    * implementation and metrics for the funding strategy 

* Quarterly releases (software and documentation) 
* Patch releases including security fixes 
* Documented funding strategy 
* Yearly User Research reports 
* 12 months & long term roadmap 

## How many hours do you estimate for these activities? We are interested in a rough estimate. Please add the hours from all developers who would be involved in this. It may be helpful to look at the hours you spent on the project in the last years. Add a number only, please.

20 000 

## Please estimate the cost of the work described in your application. It should be in numbers only, in Euros.

400 000

## In how many months will you perform the activities? enter number only

24

## Who (maintainer, contributor, organization) would be most qualified to implement this work/receive the support and why? (300)

In case of the requested grant being awarded, a new non-profit organizational structure will be established to receive the funds. The governance of the structure will be subject to the [decision making mechanism](https://codeberg.org/forgejo/governance/src/branch/main/DECISION-MAKING.md) and [code of conduct](https://forgejo.org/code-of-conduct/) of Forejo. 

This approach is necessary to supporting one of the key objectives of the proposed project: To consolidate community ownership over Forgejo, and advance the project in full publicity, which includes the transparent management of available funds and collective agreement on their distribution. 

The deliverables outlined in this proposal will be completed in the already tried and tested manner, with both volunteers and paid staff sharing the workload. The hours estimated above for completion of activities includes the time spent by volunteers. Potential selection and recruitment of new (coordination) staff, as well as remuneration of contributions will be agreed upon within the community.

# Applicant information 

## Your name/handle

Forgejo Authors

## Link to your profile (optional) (e.g. your GitHub profile)

## What is your role in this project? to tick 

Maintainer X
Contributor
Fundraiser
Fiscal host
Other

## If you are not the maintainer, are you in contact with the maintainer or the community around the technology? Do they know you plan or do this work? If yes, please provide information on how you are aligned with them. (100)

The grant proposal is a collective work of the Forgejo Authors, the community that maintains the project. It has been drafted collectively and publicly.

## Country of residence of the person who will sign the contract. This information is not relevant for selection but for administrative and evaluation purposes.

Germany 

## How did you hear about the Sovereign Tech Fund? (optional) to tick

Search engine
From another maintainer or contributor
Mailing list
GitHub
Social media (Twitter, Mastodon, LinkedIn, other)
Blog or other publication X
Podcast
YouTube
Conference
Other
